# parent class
class cars:
    
    def __init__(self):
        print("About car")

    def Price(self):
        print("List prices")

# child class
class BMW(car):

    def __init__(self):
        # call super() function
        super().__init__()
        print("BMW is one of the richest car")

    def GetPrice(self):
        print("15L")

car = BMW()
car.GetPrice();
